import dns.resolver
import sys
import time

rootServers = [ '198.41.0.4',
                '199.9.14.201',
                '192.33.4.12',
                '199.7.91.13',
                '192.203.230.10',
                '192.5.5.241',
                '192.112.36.4',
                '198.97.190.53',
                '192.36.148.17',
                '192.58.128.30',
                '193.0.14.129',
                '199.7.83.42',
                '202.12.27.33'
            ];

Type = {
    'A' : 'A',
    'NS' : 'NS',
    'MX' : 'MX'
}

rdtype = {
    "A" : 1,
    "AAAA" : 28,
    "CNAME" : 5
}

class Stack:
     def __init__(self):
         self.items = []

     def isEmpty(self):
         return self.items == []

     def push(self, item):
         self.items.append(item)

     def pop(self):
         return self.items.pop()

     def peek(self):
         return self.items[len(self.items)-1]

     def size(self):
         return len(self.items)


class RecursiveDNSResolver:

    def __init__(self, inputType):
        # init
        self.ipStack = Stack()
        # Adding Root Servers
        for rootServer in reversed(rootServers):
            self.ipStack.push(rootServer)

        self.resolvedIps = []

        self.inputType = inputType
        self.sleepFlag = 0
        self.dnsDomainQueryIndex = 0
        self.ds = {}
        self.dnsValidataionFailed = False
        self.dnsNotImplemented = False

    def addIpListToIpStack(self, ipList):
        # Adding Ips
        for ip in reversed(Ips):
            self.ipStack.push(ip)

    def printResult(self, answerIps):

        if(self.dnsValidataionFailed == True):
            print "DNSSec verification failed"
        elif(self.dnsNotImplemented == True):
            print "DNSSEC not supported"
        else :
            if(len(answerIps)>0):
                for item in answerIps.to_text().split("\n"):
                    i = item.split(" ")
                    i[0] = self.mainDomainName + '.'
                    print " ".join(i)

    def resolveDomainName(self, domain):
        self.mainDomainName = domain
        self.currentDomainName = domain
        self.dnsDomainQueryIndex = 0
        # self.currentDomainStack = 
        
        if self.inputType is Type["A"]:
            while(self.ipStack.isEmpty() == False):
                self.resolvedIps = self.queryNextLevel(self.currentDomainName, self.ipStack.pop(), 0)
                if(self.resolvedIps != [] or self.dnsValidataionFailed == True or self.dnsNotImplemented == True):
                    return self.resolvedIps
        else:
            while(self.ipStack.isEmpty() == False):
                self.resolvedIps = self.queryNextLevel(self.currentDomainName, self.ipStack.pop(), 0)
                if(self.resolvedIps != [] or self.dnsValidataionFailed == True or self.dnsNotImplemented == True):
                    return self.resolvedIps

        

    def processAnswerSection(self, answers):
        return answers

    def processAdditonalSection(self, additonal):
        for item in reversed(additonal):
            if item.rdtype is rdtype["A"]:
                self.ipStack.push(str(item[0]))

        return []

    def processAuthoritySection(self, authority):
        for item in authority[0]:
            item = str(item)
            if(item[-1:] == '.'):
                item = item[:-1]
            
            authorityRecursiveDNSResolver = RecursiveDNSResolver(self.inputType)
            for ip in authorityRecursiveDNSResolver.resolveDomainName(item):
                self.ipStack.push(str(ip))
        return []

    def processCnameAnswers(self, answers):

        cnameDomain = str(answers[0])
        if(cnameDomain[-1:] == '.'):
            cnameDomain = cnameDomain[:-1]

        cnameRecursiveDNSResolver = RecursiveDNSResolver(self.inputType)

        return cnameRecursiveDNSResolver.resolveDomainName(cnameDomain)

    def verifyDNSKeyForCurrentLevel(self, domain, ip):
        reverseIndex = self.dnsDomainQueryIndex
        answer = []
        childDNSValidataion = False

        if(reverseIndex == 0):
            tempDomain = "."
        else:
            tempDomain = domain + '.'
            tempDomain = tempDomain.split(".")

            tempDomain = tempDomain[len(tempDomain)-1-reverseIndex:]
            tempDomain =  ".".join(tempDomain)

        self.dnsDomainQueryIndex = self.dnsDomainQueryIndex + 1

        try:
            request = dns.message.make_query(tempDomain,'DNSKEY', want_dnssec=True)
            response = dns.query.udp(request,ip,timeout=2)
            answer = response.answer

        except dns.exception.Timeout:
            return []

        if reverseIndex > 0:
            childDNSValidataion = self.verifyChildDNSKey(tempDomain, response, reverseIndex)
            if childDNSValidataion == False:
                self.dnsValidataionFailed = True
                return []

        if len(answer) >= 2:

            name = dns.name.from_text(tempDomain)
            try:
                # Validating 
                dns.dnssec.validate(answer[0],answer[1],{name:answer[0]})
            except dns.dnssec.ValidationFailure:
                self.dnsValidataionFailed = True
                return []
            else:
                return answer[0]

        else :
            print "DNSKEY answer length less that 2"
            return []

    def createDomainName(self, domain):
        reverseIndex = self.dnsDomainQueryIndex - 1

        if(reverseIndex == 0):
            tempDomain = "."
        else:
            tempDomain = domain + '.'
            tempDomain = tempDomain.split(".")

            tempDomain = tempDomain[len(tempDomain)-1-reverseIndex:]
            tempDomain =  ".".join(tempDomain)

        return tempDomain
        
    # Verify the data send by the server
    def validateRRSetData(self, domain, data, dnskey):    
        if data is None:
            print "No Data"
        else:
            answer = data.answer
            authority = data.authority

            name = dns.name.from_text(self.createDomainName(domain))

            localDs = []
            rrsigDs = []

            for item in authority:
                if "IN DS" in item.to_text():
                    localDs = item
                if "IN RRSIG DS" in item.to_text():
                    rrsigDs = item

            if(localDs and rrsigDs):
                try:
                    dns.dnssec.validate(localDs,rrsigDs,{name:dnskey})
                except dns.dnssec.ValidationFailure:
                    self.dnsValidataionFailed = True
                    return False
                    # BE SUSPICIOUS
                else:
                    self.ds[self.dnsDomainQueryIndex-1] = localDs 
                    return True
                    # WE'RE GOOD, THERE'S A VALID DNSSEC SELF-SIGNED KEY FOR example.com

    def verifyChildDNSKey(self, dnsDomain, data, dsIndex):

        dnskey = None
        for item in data.answer:
            for inner_item in item:
                if "257 " == inner_item.to_text()[0:4]:
                    dnskey = inner_item

        if(dnskey == None):
            return False

        if dns.dnssec.make_ds(dnsDomain, dnskey, 'SHA256') == self.ds[self.dnsDomainQueryIndex-2][0]:
            return True
        else :
            return False
        

    def queryNextLevel(self, domain, ip, count):

        validRRSetDataFlag = False

        # Code to verify dns key
        dnskey = self.verifyDNSKeyForCurrentLevel(domain, ip)

        if(len(dnskey) == 0):
            self.dnsNotImplemented = True
            return []

        try:
            request = dns.message.make_query(domain, self.inputType, want_dnssec=True)
            data = dns.query.udp(request, ip, timeout=2);
        except dns.exception.Timeout:
            data = None
        
        #print data

        
        
        if(data and data.answer and (data.answer[0].rdtype is rdtype[Type["A"]] or self.inputType == Type["NS"] or self.inputType == Type["MX"])):
            return self.processAnswerSection(data.answer[0])
        elif(data and data.answer and data.answer[0].rdtype is rdtype["CNAME"]):
            return self.processCnameAnswers(data.answer[0])
        elif(data and data.additional and data.additional[0]):

            validRRSetDataFlag = self.validateRRSetData(domain, data, dnskey)
            if validRRSetDataFlag == False:
                return []

            return self.processAdditonalSection(data.additional)
        elif(data and data.authority): 
            
            validRRSetDataFlag = self.validateRRSetData(domain, data, dnskey)
            if validRRSetDataFlag == False:
                return []

            # print "Authority Section"
            return self.processAuthoritySection(data.authority)
        else:
            return []

if len(sys.argv) >= 2:

    inputType = Type["A"]
    if len(sys.argv) >= 3:
        inputType = str(sys.argv[2])

    domainName = sys.argv[1]

    recursiveDNSResolver = RecursiveDNSResolver(inputType)
    resolvedIps = recursiveDNSResolver.resolveDomainName(domainName)
    recursiveDNSResolver.printResult(resolvedIps)
else:
    print "Not a valid input"
