import dns.resolver
import sys
import time

rootServers = [ '198.41.0.4',
                '199.9.14.201',
                '192.33.4.12',
                '199.7.91.13',
                '192.203.230.10',
                '192.5.5.241',
                '192.112.36.4',
                '198.97.190.53',
                '192.36.148.17',
                '192.58.128.30',
                '193.0.14.129',
                '199.7.83.42',
                '202.12.27.33'
            ];

Type = {
    'A' : 'A',
    'NS' : 'NS',
    'MX' : 'MX'
}

rdtype = {
    "A" : 1,
    "AAAA" : 28,
    "CNAME" : 5
}

class Stack:
     def __init__(self):
         self.items = []

     def isEmpty(self):
         return self.items == []

     def push(self, item):
         self.items.append(item)

     def pop(self):
         return self.items.pop()

     def peek(self):
         return self.items[len(self.items)-1]

     def size(self):
         return len(self.items)


class RecursiveDNSResolver:

    def __init__(self, inputType):
        # init
        self.ipStack = Stack()
        # Adding Root Servers
        for rootServer in reversed(rootServers):
            self.ipStack.push(rootServer)

        self.resolvedIps = []

        self.inputType = inputType
        self.sleepFlag = 0

    def addIpListToIpStack(self, ipList):
        # Adding Ips
        for ip in reversed(Ips):
            self.ipStack.push(ip)

    def printResult(self, answerIps):
        print answerIps
        # print "Inside printResult"
        for item in answerIps:
            print self.mainDomainName , " ", str(item)

    def resolveDomainName(self, domain):
        self.mainDomainName = domain
        self.currentDomainName = domain
        # self.currentDomainStack = 
        
        if self.inputType is Type["A"]:
            while(self.ipStack.isEmpty() == False):
                self.resolvedIps = self.queryNextLevel(self.currentDomainName, self.ipStack.pop(), 0)
                if(self.resolvedIps != []):
                    return self.resolvedIps
        else:
            while(self.ipStack.isEmpty() == False):
                self.resolvedIps = self.queryNextLevel(self.currentDomainName, self.ipStack.pop(), 0)
                if(self.resolvedIps != []):
                    return self.resolvedIps

        

    def processAnswerSection(self, answers):
        return answers

    def processAdditonalSection(self, additonal):
        for item in reversed(additonal):
            if item.rdtype is rdtype["A"]:
                self.ipStack.push(str(item[0]))

        return []

    def processAuthoritySection(self, authority):
        for item in authority[0]:
            item = str(item)
            if(item[-1:] == '.'):
                item = item[:-1]
            
            authorityRecursiveDNSResolver = RecursiveDNSResolver(self.inputType)
            for ip in authorityRecursiveDNSResolver.resolveDomainName(item):
                self.ipStack.push(str(ip))
        return []

    def processCnameAnswers(self, answers):
        # print "###### Hello"
        # print answers
        # print answers[0]
        # print str(answers[0])[:-1]

        cnameDomain = str(answers[0])
        if(cnameDomain[-1:] == '.'):
            cnameDomain = cnameDomain[:-1]

        cnameRecursiveDNSResolver = RecursiveDNSResolver(self.inputType)

        return cnameRecursiveDNSResolver.resolveDomainName(cnameDomain)


    def queryNextLevel(self, domain, ip, count):

        # print "##### CP1 -->", domain, ip, count, self.ipStack.size()

        try:
            request = dns.message.make_query(domain,self.inputType)
            data = dns.query.udp(request, ip, timeout=2);
            print "data.question --->", data.question[0]
        except dns.exception.Timeout:
            print "$$$$$$$$$$$$$$$ Timeout Error" 
            data = None
        
        #print data
        
        if(data.answer and (data.answer[0].rdtype is rdtype[Type["A"]] or self.inputType == Type["NS"] or self.inputType == Type["MX"])):
            return self.processAnswerSection(data.answer[0])
        elif(data.answer and data.answer[0].rdtype is rdtype["CNAME"]):
            return self.processCnameAnswers(data.answer[0])
        elif(data.additional and data.additional[0]):
            return self.processAdditonalSection(data.additional)
        elif(data.authority): 
            # print "Authority Section"
            return self.processAuthoritySection(data.authority)
        else:
            print "Else Section"

    def queryNextLevelNSMX(self, domain, ip, count):

        # print "##### CP1 -->", domain, ip, count, self.ipStack.size()

        try:
            request = dns.message.make_query(domain,self.inputType)
            data = dns.query.udp(request, ip, timeout=2);
        except dns.exception.Timeout:
            print "$$$$$$$$$$$$$$$ Timeout Error" 
            data = None
        
        # print data
        print data.answer, len(data.answer)
        print data.additional, len(data.additional)
        print data.authority, len(data.authority), data.authority[0]
        print data
        print "#########################################"

        # if self.sleepFlag is 3:
        #     while(True):
        #         time.sleep(5)
        
        if(data.answer and (data.answer[0].rdtype is rdtype[Type["A"]] or self.inputType == Type["NS"] or self.inputType == Type["MX"])):
            # print "coming here --> ", self.inputType, data.answer 
            self.sleepFlag = self.sleepFlag + 1
            return self.processAnswerSection(data.answer[0])
        elif(data.answer and data.answer[0].rdtype is rdtype["CNAME"]):
            return self.processCnameAnswers(data.answer[0])
        elif(data.additional and data.additional[0]):
            self.sleepFlag = self.sleepFlag + 1
            return self.processAdditonalSection(data.additional)
        elif(data.authority): 
            # print "Authority Section"
            return self.processAuthoritySection(data.authority)
        else:
            print "Else Section"

if len(sys.argv) >= 2:

    inputType = Type["A"]
    if len(sys.argv) >= 3:
        inputType = str(sys.argv[2])

    domainName = sys.argv[1]

    recursiveDNSResolver = RecursiveDNSResolver(inputType)
    resolvedIps = recursiveDNSResolver.resolveDomainName(domainName)
    recursiveDNSResolver.printResult(resolvedIps)
else:
    print "Not a valid input"
