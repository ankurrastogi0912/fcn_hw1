import dns.resolver
import sys
import time

rootServers = [ '198.41.0.4',
                '199.9.14.201',
                '192.33.4.12',
                '199.7.91.13',
                '192.203.230.10',
                '192.5.5.241',
                '192.112.36.4',
                '198.97.190.53',
                '192.36.148.17',
                '192.58.128.30',
                '193.0.14.129',
                '199.7.83.42',
                '202.12.27.33'
            ];

Type = {
    'A' : 'A',
    'NS' : 'NS',
    'MX' : 'MX'
}

rdtype = {
    "A" : 1,
    "AAAA" : 28,
    "CNAME" : 5
}

class Stack:
     def __init__(self):
         self.items = []

     def isEmpty(self):
         return self.items == []

     def push(self, item):
         self.items.append(item)

     def pop(self):
         return self.items.pop()

     def peek(self):
         return self.items[len(self.items)-1]

     def size(self):
         return len(self.items)


class RecursiveDNSResolver:

    def __init__(self, inputType):
        # init
        self.ipStack = Stack()
        # Adding Root Servers
        for rootServer in reversed(rootServers):
            self.ipStack.push(rootServer)

        self.resolvedIps = []

        self.inputType = inputType
        self.sleepFlag = 0
        self.question = ""
        self.messageSize = 0
        self.stopSearchFlag = False


    def addIpListToIpStack(self, ipList):
        # Adding Ips
        for ip in reversed(Ips):
            self.ipStack.push(ip)

    def printResult(self, answerIps, runningTime):
        print ""
        print "Question Section:"
        print self.question
        print ""
        print "Answer Section:"
        if(len(answerIps)>0):
            for item in answerIps.to_text().split("\n"):
                i = item.split(" ")
                i[0] = self.mainDomainName + '.'
                print " ".join(i)
        else:
            print ""
        print ""
        print "Query time:", int(runningTime*1000), "msec"
        print "WHEN:", time.ctime()
        print "MSG SIZE rcvd:", self.messageSize
        print ""

    def resolveDomainName(self, domain):
        self.mainDomainName = domain
        self.currentDomainName = domain
        # self.currentDomainStack = 
        
        if self.inputType is Type["A"]:
            while(self.ipStack.isEmpty() == False):
                self.resolvedIps = self.queryNextLevel(self.currentDomainName, self.ipStack.pop(), 0)
                if(self.resolvedIps != [] or self.stopSearchFlag == True):
                    return self.resolvedIps
        else:
            while(self.ipStack.isEmpty() == False):
                self.resolvedIps = self.queryNextLevel(self.currentDomainName, self.ipStack.pop(), 0)
                if(self.resolvedIps != [] or self.stopSearchFlag == True):
                    return self.resolvedIps

        

    def processAnswerSection(self, answers):
        return answers

    def processAdditonalSection(self, additonal):
        for item in reversed(additonal):
            if item.rdtype is rdtype["A"]:
                self.ipStack.push(str(item[0]))

        return []

    def processAuthoritySection(self, authority):
        for item in authority[0]:
            item = str(item)
            if(item[-1:] == '.'):
                item = item[:-1]
            
            authorityRecursiveDNSResolver = RecursiveDNSResolver(self.inputType)
            ips = authorityRecursiveDNSResolver.resolveDomainName(item)
            if ips == None:
                return []
            for ip in ips:
                self.ipStack.push(str(ip))
        return []

    def processCnameAnswers(self, answers):
        # print "###### Hello"
        # print answers
        # print answers[0]
        # print str(answers[0])[:-1]

        cnameDomain = str(answers[0])
        if(cnameDomain[-1:] == '.'):
            cnameDomain = cnameDomain[:-1]

        cnameRecursiveDNSResolver = RecursiveDNSResolver(self.inputType)

        return cnameRecursiveDNSResolver.resolveDomainName(cnameDomain)


    def queryNextLevel(self, domain, ip, count):

        # print "##### CP1 -->", domain, ip, count, self.ipStack.size()

        try:
            request = dns.message.make_query(domain,self.inputType)
            data = dns.query.udp(request, ip, timeout=2);
            self.question = data.question[0]
        except dns.exception.Timeout:
            data = None
        
        if data:
            self.messageSize = sys.getsizeof(str(data.answer)) + sys.getsizeof(str(data.authority)) + sys.getsizeof(str(data.additional))

        #print data
        
        if(data and data.answer and (data.answer[0].rdtype is rdtype[Type["A"]] or self.inputType == Type["NS"] or self.inputType == Type["MX"])):
            return self.processAnswerSection(data.answer[0])
        elif(data and data.answer and data.answer[0].rdtype is rdtype["CNAME"]):
            return self.processCnameAnswers(data.answer[0])
        elif(data and data.additional and data.additional[0]):
            return self.processAdditonalSection(data.additional)
        elif(data and data.authority): 
            if(len(data.authority) > 0):
                if(data.authority[0].rdtype == 6):
                    self.stopSearchFlag = True
                    return []
                else:
                    return self.processAuthoritySection(data.authority)
            else :
                return [] 
        else:
            return []

if len(sys.argv) >= 2:

    inputType = Type["A"]
    if len(sys.argv) >= 3:
        inputType = str(sys.argv[2])
        if(inputType != Type["NS"] and inputType != Type["MX"] and inputType != Type["A"]):
            print "Improper input type passes, Taking input as 'A'"
            print ""
            inputType = Type["A"]

    domainName = sys.argv[1]

    startTime = time.time()
    recursiveDNSResolver = RecursiveDNSResolver(inputType)
    resolvedIps = recursiveDNSResolver.resolveDomainName(domainName)
    doneTime = time.time()
    recursiveDNSResolver.printResult(resolvedIps, doneTime-startTime)
else:
    print "Not a valid input"

# top25Sites = ['google.com','youtube.com',
#               'facebook.com','baidu.com',
#               'wikipedia.org','reddit.com',
#               'yahoo.com','google.co.in',
#               'qq.com','taobao.com',
#               'amazon.com','tmall.com',
#               'twitter.com','google.co.jp',
#               'instagram.com','live.com',
#               'vk.com','sohu.com',
#               'sina.com.cn','jd.com',
#               'weibo.com','360.cn',
#               'google.de','google.co.ck',
#               'google.com.br'];

# top25AvgTimes = {}
# cummalativeTime = 0
# siteCount = 0
# for site in top25Sites:
#     startTime = time.time()
#     for i in range(0,10):
#         recursiveDNSResolver = RecursiveDNSResolver(Type["A"])
#         recursiveDNSResolver.resolveDomainName(site)
#         print "#################"
#         # time.sleep(1)
#     doneTime = time.time()
#     cummalativeTime = (doneTime-startTime)/10
#     top25AvgTimes[siteCount] = cummalativeTime
#     siteCount = siteCount + 1
#     # time.sleep(1)
#     print "####################################################################"

# print top25AvgTimes
# print cummalativeTime
