import dns.name
import dns.query
import dns.dnssec
import dns.message
import dns.resolver
import dns.rdatatype

rootServers = [	'198.41.0.4',
				'199.9.14.201',
				'192.33.4.12',
				'199.7.91.13',
				'192.203.230.10',
				'192.5.5.241',
				'192.112.36.4',
				'198.97.190.53',
				'192.36.148.17',
				'192.58.128.30',
				'193.0.14.129',
				'199.7.83.42',
				'202.12.27.33'
			];

# # get nameservers for target domain
# response = dns.resolver.query('example.com.','A')

# # we'll use the first nameserver in this example
# nsname = response.rrset[0] # name
# response = dns.resolver.query(nsname,dns.rdatatype.A)
# nsaddr = response.rrset[0].to_text() # IPv4

# print nsname, response, nsaddr

# get DNSKEY for zone
request = dns.message.make_query('.','DNSKEY', want_dnssec=True)

# send the query
response = dns.query.udp(request,'198.41.0.4')
# if response.rcode() != 0:
    # HANDLE QUERY FAILED (SERVER ERROR OR NO DNSKEY RECORD)

# answer should contain two RRSET: DNSKEY and RRSIG(DNSKEY)
answer = response.answer

print response
print response.answer
print response.authority
print response.additional

# if len(answer) != 2:
    # SOMETHING WENT WRONG

# the DNSKEY should be self signed, validate it
name = dns.name.from_text('example.com')
try:
    dns.dnssec.validate(answer[0],answer[1],{name:answer[0]})
except dns.dnssec.ValidationFailure:
	print "Tested"
    # BE SUSPICIOUS
else:
	print "Failed"
    # WE'RE GOOD, THERE'S A VALID DNSSEC SELF-SIGNED KEY FOR example.com

# first we need to call the root server
# If it doesnt provide Anwer.
# Then I will check _____ section
# make request to next server
# then it will give me name servers
# Then I will query the name server to find the answer

# answer = dns.resolver.query('dnspython.org','A')

# request = dns.message.make_query('stonybrook.edu',dns.rdatatype.DNSKEY,want_dnssec=True)
# data1 = dns.query.udp(request, '198.41.0.4');

# name = dns.name.from_text('www.google.com')

# print name

# # data2 = dns.query.udp(request, '192.5.6.30');

# # data3 = dns.query.udp(request, '216.239.34.10');

# print data1.to_text();

# # print "########################"

# # print data2.to_text();

# # print "########################"

# # print data3.to_text();

# # print "########################"

# # print data1.additional[0]

# # print "########################"

# # print data3.answer[0]

# print "########################"

# print data1.payload