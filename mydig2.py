import dns.name
import dns.query
import dns.dnssec
import dns.message
import dns.resolver
import dns.rdatatype
import hashlib
import base64

rootServers = [	'198.41.0.4',
				'199.9.14.201',
				'192.33.4.12',
				'199.7.91.13',
				'192.203.230.10',
				'192.5.5.241',
				'192.112.36.4',
				'198.97.190.53',
				'192.36.148.17',
				'192.58.128.30',
				'193.0.14.129',
				'199.7.83.42',
				'202.12.27.33'
			];

#################################################################################

# get DNSKEY for zone
request = dns.message.make_query('.','DNSKEY', want_dnssec=True)

# send the query
response = dns.query.udp(request,'198.41.0.4')
# if response.rcode() != 0:
    # HANDLE QUERY FAILED (SERVER ERROR OR NO DNSKEY RECORD)

# answer should contain two RRSET: DNSKEY and RRSIG(DNSKEY)
answer = response.answer

# print response
# print response.answer
# print response.authority
# print response.additional

# if len(answer) != 2:
    # SOMETHING WENT WRONG

# the DNSKEY should be self signed, validate it
name = dns.name.from_text('www.huque.com')
try:
    dns.dnssec.validate(answer[0],answer[1],{name:answer[0]})
except dns.dnssec.ValidationFailure:
	print "Tested"
	print "#################################################################################"
    # BE SUSPICIOUS
else:
	print "Failed"

#################################################################################

request1 = dns.message.make_query('www.huque.com','A', want_dnssec=True)

# send the query
response1 = dns.query.udp(request1,'198.41.0.4')
# if response.rcode() != 0:
    # HANDLE QUERY FAILED (SERVER ERROR OR NO DNSKEY RECORD)

# answer should contain two RRSET: DNSKEY and RRSIG(DNSKEY)
answer1 = response1.answer
authority1 = response1.authority

# print response1
# print response1.answer
# print response1.authority, len(response1.authority[0]), len(response1.authority[1]), len(response1.authority[2])
# print ""
# print ""
# print response1.authority[0].to_text()
# print ""
# print ""
# print response1.authority[1].to_text()
# print ""
# print ""
# print response1.authority[2].to_text()
# print ""
# print ""
# print response1.additional

# if len(answer) != 2:
    # SOMETHING WENT WRONG

# the DNSKEY should be self signed, validate it
name1 = dns.name.from_text('www.huque.com')
try:
    dns.dnssec.validate(authority1[1],authority1[2],{name1:answer[0]})
except dns.dnssec.ValidationFailure:
	print "Tested"
	print "#################################################################################"
    # BE SUSPICIOUS
else:
	print "Failed"
    # WE'RE GOOD, THERE'S A VALID DNSSEC SELF-SIGNED KEY FOR example.com

ds = authority1[1]

#################################################################################



# get DNSKEY for zone
request2 = dns.message.make_query('com.','DNSKEY', want_dnssec=True)

# send the query
response2 = dns.query.udp(request2,'192.12.94.30')
# if response.rcode() != 0:
    # HANDLE QUERY FAILED (SERVER ERROR OR NO DNSKEY RECORD)

# answer should contain two RRSET: DNSKEY and RRSIG(DNSKEY)
answer2 = response2.answer

# the DNSKEY should be self signed, validate it
name2 = dns.name.from_text('www.huque.com')

# print response2
# print response2.answer, len(response2.answer[0]), len(response2.answer[1])
# print response2.authority
# print response2.additional

# print "####### 1", response2.answer[0][0]
print "####### 2", response2.answer[0][1]
# print "####### 3", response2.answer[1][0]

print "####### 4", dns.dnssec.make_ds("com.", response2.answer[0][1], 'SHA256') #hashlib.sha256(response2.answer[0][1].to_text().encode()).hexdigest()
print "####### 5", ds[0].to_text()


try:
    dns.dnssec.validate(answer2[0],answer2[1],{name2:answer2[0]})
except dns.dnssec.ValidationFailure:
	print "Tested"
	print "#################################################################################"
    # BE SUSPICIOUS
else:
	print "Failed"

# ################################################################################

request3 = dns.message.make_query('www.huque.com','A', want_dnssec=True)

# send the query
response3 = dns.query.udp(request3,'192.12.94.30')
# if response.rcode() != 0:
    # HANDLE QUERY FAILED (SERVER ERROR OR NO DNSKEY RECORD)

# answer should contain two RRSET: DNSKEY and RRSIG(DNSKEY)
answer3 = response3.answer
authority3 = response3.authority

# print response3
# print response3.answer
# print response3.authority, len(response3.authority[0]), len(response3.authority[1]), len(response3.authority[2])
# print ""
# print ""
# print response3.authority[0].to_text()
# print ""
# print ""
# print response3.authority[1].to_text()
# print ""
# print ""
# print response3.authority[2].to_text()
# print ""
# print ""
# print response3.additional

# if len(answer) != 2:
#     SOMETHING WENT WRONG

# the DNSKEY should be self signed, validate it
name3 = dns.name.from_text('www.huque.com')
try:
    dns.dnssec.validate(authority3[1],authority3[2],{name3:answer2[0]})
except dns.dnssec.ValidationFailure:
	print "Tested"
	print "#################################################################################"
    # BE SUSPICIOUS
else:
	print "Failed"
    # WE'RE GOOD, THERE'S A VALID DNSSEC SELF-SIGNED KEY FOR example.com

ds1 = authority3[1]


# #################################################################################



# get DNSKEY for zone
request4 = dns.message.make_query('huque.com.','DNSKEY', want_dnssec=True)

# send the query
response4 = dns.query.udp(request4,'128.91.254.22')
# if response.rcode() != 0:
    # HANDLE QUERY FAILED (SERVER ERROR OR NO DNSKEY RECORD)

# answer should contain two RRSET: DNSKEY and RRSIG(DNSKEY)
answer4 = response4.answer

# print response4
# print response4.answer, len(response4.answer[0]), len(response4.answer[1])
# print response4.authority
# print response4.additional

# print "####### 1", response4.answer[0][0]
# print "####### 2", response4.answer[0][1]
# print "####### 3", response4.answer[1][0]
# print "####### 4", response4.answer[1][1]

# print "####### 4", dns.dnssec.make_ds("huque.com.", response4.answer[0][1], 'SHA256')
# print "####### 5", ds1[0].to_text()

# the DNSKEY should be self signed, validate it
name4 = dns.name.from_text('www.huque.com')
try:
    dns.dnssec.validate(answer4[0],answer4[1],{name4:answer4[0]})
except dns.dnssec.ValidationFailure:
	print "Tested"
	print "#################################################################################"
    # BE SUSPICIOUS
else:
	print "Failed"

# ################################################################################

request5 = dns.message.make_query('www.huque.com','A', want_dnssec=True)

# send the query
response5 = dns.query.udp(request5,'128.91.254.22')
# if response.rcode() != 0:
    # HANDLE QUERY FAILED (SERVER ERROR OR NO DNSKEY RECORD)

# answer should contain two RRSET: DNSKEY and RRSIG(DNSKEY)
answer5 = response5.answer
authority5 = response5.authority

print response5
print response5.answer
print response5.authority, len(response5.authority[0]), len(response5.authority[1])
print response5.additional

# # if len(answer) != 2:
#     # SOMETHING WENT WRONG

# # the DNSKEY should be self signed, validate it
# name5 = dns.name.from_text('www.huque.com')
# try:
#     dns.dnssec.validate(authority5[0],authority5[1],{name5:answer4[0]})
# except dns.dnssec.ValidationFailure:
# 	print "Tested"
# 	print "#################################################################################"
#     # BE SUSPICIOUS
# else:
# 	print "Failed"
#     # WE'RE GOOD, THERE'S A VALID DNSSEC SELF-SIGNED KEY FOR example.com

# ds2 = authority5[0]