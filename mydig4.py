import dns.resolver

rootServers = [	'198.41.0.4',
				'199.9.14.201',
				'192.33.4.12',
				'199.7.91.13',
				'192.203.230.10',
				'192.5.5.241',
				'192.112.36.4',
				'198.97.190.53',
				'192.36.148.17',
				'192.58.128.30',
				'193.0.14.129',
				'199.7.83.42',
				'202.12.27.33'
			];

# domainName = "www.google.com"
# domainName = "taobao.com"
# domainName = "www.yahoo.com"
domainName = "www.facebook.com"
# domainName = "www.facebook.com"
# domainName = "atsv2-fp.wg1.b.yahoo.com"

Type = {
	"A" : 'A',
	"NS" : 'NS',
	"MX" : 'MX'
};

rdtype = {
	"A" : 1,
	"AAAA" : 28,
	"CNAME" : 5
}

def queryNextLevel(domain, ip,count):

	# print "###################################### CP8 --> "
	# print "domain -->", domain
	# print "ip---->", ip
	# print "count--->", count

	if(domain[len(domain)-1] == '.'):
		domain = domain[:-1]

	try:
		request = dns.message.make_query(domain,Type["A"])
		data = dns.query.udp(request, ip, timeout=2);
	except dns.exception.Timeout:
		print "$$$$$$$$$$$$$$$ Timeout Error" 
		data = None

	prevCount = count;
	count = count + 1;

	if(data):
		if(data.answer):
			print "###################################### CP2 --> ", len(data.answer), len(data.answer[0])
			# print data.to_text()
			# print ""
		if(data.answer and data.answer[0].rdtype is rdtype["A"]):
			for k in range(0, len(data.answer[0])) :
				print "Server Answ" + str(prevCount) + "-->", data.answer[0][k]
				global cNameDomainIp
				cNameDomainIp = data.answer[0][k]

			print "###################################### CP3 --> ", data.answer[0].rdtype
			print "###################################### CP4 --> ", len(data.answer)
			return True
		elif(data.answer and data.answer[0].rdtype is rdtype["CNAME"]):
			# request = dns.message.make_query(data.answer[0].to_text().split(" ")[4],Type["A"])
			# data = dns.query.udp(request, ip);
			print "###################################### CP5 --> ", data.answer[0].to_text(), len(data.answer)
			if(queryRootServers(data.answer[0].to_text().split(" ")[4]) == True):
				return True
		elif(data.additional and data.additional[0]):
			for j in range(0, len(data.additional[0])):
				if data.additional[j].rdtype is rdtype["A"]:
					print "Server Pass" + str(prevCount) + "-->", data.additional[j]
					if(queryNextLevel(domain, str(data.additional[0][j]),count) == True):
						return True
		elif(data.authority):
			# return True
			print "###################################### CP6 --> ",len(data.authority), len(data.authority[0])
			for j in range(0, len(data.authority[0])):
				if(queryRootServers(str(data.authority[0][j])) == True):
					print "data.authority --> ", domainName, " --> ", cNameDomainIp
					# return True
					if(queryNextLevel(domainName, str(cNameDomainIp), count + 1) == True):
						return True
			# return True
		else:
			print "###################################### CP7 --> ", data
			return True


def queryRootServers(domain):
	for i in range(0, len(rootServers)):
		print "###################################### CP1 --> ", rootServers[i], i
		if(queryNextLevel(domain, rootServers[i], 0) == True):
			return True

queryRootServers(domainName)

'''
Queries
1. What to do when additional and answer both are empty
'''
